package dawson;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void echoTest()
    {
        App a = new App();
        assertEquals("Checks if the echo method returns the correct value", 5, a.echo(5));
    }
    @Test
    public void oneMoreTest()
    {
        App a = new App();
        assertEquals("Checks if the oneMore method returns the correct value", 6, a.oneMore(5));
    }
}
